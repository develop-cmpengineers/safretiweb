<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Admin'], function () {

    // why use name
    Route::get('admin/home', 'HomeController@index')->name('admin.name');

    // City route
    Route::resource('admin/city', 'CityController');

    // Country route
    Route::resource('admin/country', 'CountryController');

    // Place route
    Route::resource('admin/place', 'PlaceController');

    // Restaurant route
    Route::resource('admin/restaurant', 'RestaurantController');

    // Shop route
    Route::resource('admin/shop', 'ShopController');

    // Admin Route Login
    Route::get('admin-login', 'Auth\LoginController@showLoginForm')->name('admin.login');

    Route::post('admin-login','Auth\LoginController@login');

    // for drop down list depend on which country did client select
    Route::get('city/get/{id}', 'CityController@getCities');
    

});



Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', function (){

    return view('welcome');

});
