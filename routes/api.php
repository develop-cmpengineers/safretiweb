<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



//Route::get('cities', 'Admin\Api\ApiController@cities');
//Route::get('city/{id}', 'Admin\Api\ApiController@city');
//
//Route::get('countries', 'Admin\Api\ApiController@countries');
//Route::get('country/{id}', 'Admin\Api\ApiController@country');
//
//Route::get('places', 'Admin\Api\ApiController@places');


Route::group(['namespace' => 'Admin\Api'], function (){

   Route::get('cities', 'ApiController@cities');
   Route::get('city/{id}', 'ApiController@city');

   //Route::post('place_city_country/','ApiController@place_city_country');

    Route::get('citiesByCountry/{country_id}', 'ApiController@citiesByCountry');

   Route::get('place/image', 'ApiController@placeImage');

   Route::get('countries','ApiController@countries');
   Route::get('country/{id}','ApiController@country');

   Route::get('places','ApiController@places');
   Route::get('place/{id}','ApiController@place');

   Route::get('restaurants','ApiController@restaurants');
   Route::get('restaurant/{id}','ApiController@restaurant');

   Route::get('shops','ApiController@shops');
   Route::get('shop/{id}','ApiController@shop');



    Route::get('placesByCityCountry/{city_id}/{country_id}','ApiController@placesByCityCountry');
    Route::get('shopsByCityCountry/{city_id}/{country_id}','ApiController@shopsByCityCountry');
    Route::get('restaurantsByCityCountry/{city_id}/{country_id}','ApiController@restaurantsByCityCountry');

    Route::post('/shopbycitycountry','ApiController@shopbycitycountry');


});

