<?php

namespace App\Http\Controllers\Admin;

use App\Model\Admin\City;
use App\Model\Admin\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Shop;

class ShopController extends Controller
{

    public function __construct()
    {
        return $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $shops = Shop::all();
        $countries = Country::all();
        $cities = City::all();
        return view('admin.shop.index',compact('shops', 'countries','cities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $countries = Country::all();
        $cities    = City::all();
        return view('admin.shop.create',compact('countries','cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [

            'name' => 'required',
            'address' => 'required',
            'desc' => 'required',

        ]);

        if ($request->hasFile('image')){
            $shopImage = $request->image->store('public/images');
        } else {

            $shopImage = '';
        }

        $shop = new Shop;

        $shop->image        = $shopImage;
        $shop->name         = $request->name;
        $shop->address      = $request->address;
        $shop->desc         = $request->desc;
        $shop->mobile       = $request->mobile;
        $shop->country_id = $request->country;
        $shop->city_id    = $request->city;

        $shop->save();

        return redirect(route('shop.index'));


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $shop = Shop::where('id',$id)->first();
        $cities = City::all();
        $countries = Country::all();

        return view('admin.shop.edit', compact('shop','cities','countries'));


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request,[

            'name' => 'required',
            'address' => 'required',
            'desc' => 'required',

        ]);


        $shop = Shop::find($id);


        $shop->name     = $request->name;
        $shop->address  = $request->address;
        $shop->desc     = $request->desc;
        $shop->mobile   = $request->mobile;
        $shop->country_id = $request->country;
        $shop->city_id    = $request->city;

        if ($request->hasFile('image')){
            $shopImage =  $request->image->store('public/images');
            $shop->image   = $shopImage;
           }


        $shop->save();

        return redirect(route('shop.index'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Shop::where('id',$id)->delete();

        return redirect()->back();
    }
}
