<?php

namespace App\Http\Controllers\Admin;

use App\Model\Admin\City;
use App\Model\Admin\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Restaurant;
use Illuminate\Support\Facades\DB;

class RestaurantController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $restaurants = Restaurant::all();
        $countries = Country::all();
        $cities = City::all();
        return view('admin.restaurant.index', compact('restaurants', 'countries','cities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
       // $countries = Country::all();
       // $cities    = City::all();

       // abort_unless(\Gate::allows('restaurant_create'), 401);
        //$countries = Country::all()->pluck('name','id')->prepend(trans('global.pleaseSelect'), '');
        $countries = DB::table('countries')->pluck('name', 'id');

        return view('admin.restaurant.create', compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [

            'name' => 'required',
            'address' => 'required',
            'desc' =>'required',


        ]);

            if ($request->hasFile('image')) {
               $restaurantImage = $request->image->store('public/images');
            } else {

                $restaurantImage = '';
            }

            $restaurant = new Restaurant;

            $restaurant->image         = $restaurantImage;
            $restaurant->name         = $request->name;
            $restaurant->address      = $request->address;
            $restaurant->desc         = $request->desc;
            $restaurant->mobile       = $request->mobile;
            $restaurant->country_id = $request->country;
            $restaurant->city_id    = $request->city;


            $restaurant->save();

            return redirect(route('restaurant.index'));


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $restaurant = Restaurant::where('id',$id)->first();

        $cities  = City::all();
        $countries = Country::all();

        return view('admin.restaurant.edit', compact('restaurant', 'cities','countries'));


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $this->validate($request, [

                'name' => 'required',
                'address' => 'required',
                'desc' =>  'required',
                'mobile' => 'required',

        ]);



        $restaurant = Restaurant::find($id);


        $restaurant->name           = $request->name;
        $restaurant->address        = $request->address;
        $restaurant->desc           = $request->desc;
        $restaurant->mobile         = $request->mobile;
        $restaurant->country_id     = $request->country;
        $restaurant->city_id        = $request->city;

        if ($request->hasFile('image')){
            $restaurantImage = $request->image->store('public/images');
            $restaurant->image = $restaurantImage;
        }


        $restaurant->save();

        return redirect(route('restaurant.index'));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        Restaurant::where('id',$id)->delete();

        return redirect()->back();
    }

}
