<?php

namespace App\Http\Controllers\Admin\Api;

use App\Model\Admin\Country;
use App\Model\Admin\Place;
use App\Model\Admin\Restaurant;
use App\Model\Admin\Shop;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\City;


class ApiController extends Controller
{
    public function cities()
    {
        return City::all();
    }

    public function city($id)
    {
        $city =  City::where('id', $id)->get();

        return $city;

    }

    public function countries()
    {
        return Country::all();
    }

    public function country($id)
    {
        return Country::where('id', $id)->get();
    }

    public function citiesByCountry($country_id)
    {

        return City::where(['country_id' => $country_id])->get();

       // return 'Hello';
    }



    public function places()
    {
        return Place::all();

    }

    public function place($id)
    {
        return Place::where('id',$id)->get();
    }


    public function restaurants()
    {
        return Restaurant::all();
    }

    public function restaurant($id)
    {
        return Restaurant::where('id',$id)->get();
    }

    public function shops()
    {
        return Shop::all();
    }

    public function shop($id)
    {
        return Shop::where('id',$id)->get();
    }

    public function placesByCityCountry($city_id,$country_id)
    {

        return Place::where(['city_id' =>$city_id, 'country_id' => $country_id])->get();

    }


    public function restaurantsByCityCountry($city_id, $country_id)
    {

        return Restaurant::where(['city_id'=> $city_id , 'country_id' => $country_id])->get();

    }

    public function shopsByCityCountry($city_id,$country_id)
    {

        return Shop::where(['city_id' => $city_id , 'country_id' => $country_id])->get();
    }

    // POST api Request
    public function shopbycitycountry(Request $request)
    {
        $input = $request->all();

       return  Shop::where(['city_id' => $input['country_id'], 'country_id'=> $input['country_id'] ])->get();


    }

}
