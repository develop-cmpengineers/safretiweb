<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\City;
use App\Model\Admin\Country;
use Illuminate\Support\Facades\DB;

class CityController extends Controller
{

    public function __construct()
    {
        return $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $cities = City::all();
        $countries = Country::all();
        return view('admin.city.index', compact('cities', 'countries'));

       // return City::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $countries = Country::all();

        return view('admin.city.create', compact('countries'));


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {



        //
        $this->validate($request, [

            'name' => 'required'

        ]);

            $city = new City;
            $city->name = $request->name;

            $country = new Country;
            $country->id = $request->country;

            $city->save();
            $city->country()->associate($country->id)->save();

            return redirect(route('city.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $city = City::where('id',$id)->first();
        $countries = Country::all();

        return view('admin.city.edit', compact('city', 'countries'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [

            'name' => 'required'

        ]);

        $city = City::find($id);

        $city->name = $request->name;

        $city->country_id = $request->country;

        $city->save();

        return redirect(route('city.index'));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        City::where('id',$id)->delete();

        return redirect()->back();

    }

    // fetching all from cities table by passing country_id
    public function getCities($id)
    {
        $cities = DB::table("cities")->where('country_id', $id)->pluck('name', 'id');

        return json_encode($cities);
    }

}
