<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Place;
use App\Model\Admin\Country;
use App\Model\Admin\City;

class PlaceController extends Controller
{

    // public function __construct()
    // {
    //     return $this->middleware('auth:admin');
    // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $places = Place::all();
        $countries = Country::all();
        $cities = City::all();

        return view('admin.place.index', compact('places','countries','cities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $countries = Country::all();
        $cities = City::all();

        return view('admin.place.create',compact('countries','cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        //
        $this->validate($request, [

            'name' => 'required',
            'address' => 'required',
            'desc' => 'required',

        ]);

        if ($request->hasFile('image')) {

          $imagePlace =  $request->image->store('public/images');

        } else {

            $imagePlace = '';
        }

        $place = new Place;
        $place->image        = $imagePlace;
        $place->name         = $request->name;
        $place->address      = $request->address;
        $place->desc         = $request->desc;
        $place->mobile       = $request->mobile;
        $place->country_id = $request->country;
        $place->city_id    = $request->city;


        $place->save();







        return redirect(route('place.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $place     = Place::where('id', $id)->first();
        $countries = Country::all();
        $cities     = City::all();

        return view('admin.place.edit', compact('place', 'countries','cities'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [

            'name'       => 'required',
            'address'    => 'required',
            'desc'       => 'required',

            'mobile'     => 'required',


        ]);



        $place = Place::find($id);



        $place->name       = $request->name;
        $place->address    = $request->address;
        $place->desc       = $request->desc;
        $place->mobile     = $request->mobile;
        $place->country_id = $request->country;
        $place->city_id    =  $request->city;

        if ($request->hasFile('image')) {
            $imagePlace = $request->image->store('public/images');
            $place->image = $imagePlace;
        }

        $place->save();

        return redirect(route('place.index'));


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Place::where('id',$id)->delete();

        return redirect()->back();

    }
}
