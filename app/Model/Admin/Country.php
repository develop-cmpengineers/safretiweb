<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    //
    protected $fillable = [

        'name', 'id'
    ];


    public function cities()
    {
        return $this->hasMany(City::class);
    }


}
