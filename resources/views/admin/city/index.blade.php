@extends('admin.layouts.app')

@section('headSection')

 <link rel="stylesheet" href="{{ asset('admin/plugins/datatables/dataTables.bootstrap.css') }}">

@endsection

@section('main-content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Blank page
            <small>it all starts here</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Blank page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Title</h3>
                    <a type="button" class="btn btn-success" href="{{ route('city.create') }}">Add City</a>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>S.No</th>
                            <th>City </th>
                            <th>Country</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>



                    <tbody>
                        @foreach($cities as $city)
                        <tr>
                            <td>{{ $loop->index + 1 }}</td>
                            <td>{{ $city->name }}</td>
                            <td>
                                @foreach($countries as $country)
                                    @if($city->country_id == $country->id)
                                    {{ $country->name }}
                                    @endif
                                    @endforeach

                            </td>
                            <td> <a href="{{ route('city.edit', $city->id) }}"><span class="glyphicon glyphicon-edit"></span></a></td>
                            <td> <form action="{{ route('city.destroy', $city->id) }}" method="POST" id="delete-form-{{ $city->id }}">

                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                            </form>
                            <a  onclick="
                                if(confirm('Are you sure you want to delete this ?')) {
                                    event.preventDefault();
                                    document.getElementById('delete-form-{{ $city->id }}').submit();
                                } else{
                                    event.preventDefault();
                                }

                            ">
                                <span class="glyphicon glyphicon-trash"></span>
                            </a>

                            </td>
                        </tr>
                        @endforeach
                    </tbody>

                    <th>S.No</th>
                    <th>City</th>
                    <th>Country</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                Footer
            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('footerSection')

<script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });

</script>

@endsection
