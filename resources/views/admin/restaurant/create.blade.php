@extends('admin.layouts.app')



@section('main-content')


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
          <h1>
              Restaurants
              <small>safreti</small>
          </h1>
          <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li><a href="#">Forms</a></li>
              <li class="active">Editors</li>
          </ol>
      </section>

      <!-- Main content -->
      <section class="content">
          <div class="row">
              <div class="col-md-12">

                  <div class="box box-primary">
                      <div class="box-header with-border">
                          <h3 class="box-title">Titles</h3>

                      </div>

                      @include('includes.messages')

                      <!-- /.box-header -->
                      <!-- form start -->
                      <form role="form" method="POST" action="{{ route('restaurant.store') }}" enctype="multipart/form-data">
                          <div class="box-body">
                              <div class="col-lg-12">

                                {{ csrf_field() }}



                                  <div class="form-group">
                                      <label for="title">Restaurant Name </label>
                                      <input type="text" class="form-control" id="title" name="name" placeholder="Enter Restaurant" >
                                  </div>


                                  <div class="form-group">
                                      <label for="sub title">Restaurant address</label>
                                      <input type="text" class="form-control" id="address" name="address" placeholder="Enter Address" >
                                  </div>

                                  <div class="form-group">
                                      <label for="slug">Restaurant Description </label>
                                      <input type="text" class="form-control" id="desc" name="desc" placeholder="Enter Description" >
                                  </div>

                                  <div class="form-group">
                                    <label for="slug">Restaurant Mobile </label>
                                    <input type="text" class="form-control" id="desc" name="mobile" placeholder="Enter Mobile" >
                                </div>


                                <div class="form-group">
                                        <label>Select Country</label>
                                        <select name="country" class="form-control" id="country">
                                            <option value="">Please Select</option>
{{--                                         @foreach($countries as $country)--}}
{{--                                          <option value="{{ $country->id }}"--}}

{{--                                             > {{ $country->name }}</option>--}}

{{--                                          @endforeach--}}
{{--                                        </select>--}}
                                            @foreach($countries as $country => $value)
                                            <option value="{{ $country }}">{{ $value }}</option>
                                                @endforeach
                                        </select>
                                      </div>


                                      <div class="form-group">
                                            <label>Select City</label>
                                            <select name="city" class="form-control" id="city">
                                                <option>Please Select</option>
{{--                                              @foreach($cities as $city)--}}

{{--                                                            @if( $country->id == $city->country_id)--}}

{{--                                                        <option value="{{ $city->id }}"--}}
{{--                                                            @endif--}}

{{--                                                >{{ $city->name }}</option>--}}

{{--                                                @endforeach--}}
                                                <option>-- State -- </option>

                                            </select>
                                          </div>

                                  <div class="form-group">
                                    <label for="image">Restaurant Image</label>
                                    <input type="file" name="image" id="image">

                                    <p class="help-block">Example block-level help text here.</p>
                                </div>

                            <div class="form-group">
                              <button type="submit" class="btn btn-primary">Submit</button>
                              <a type="button" class="btn btn-warning" href="{{ route('restaurant.index') }}">Back</a>
                          </div>
                              </div>
                          </div>
                      </form>
                  </div>

              </div>
              <!-- /.col-->
          </div>
          <!-- ./row -->
      </section>
      <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection


@section('footerSection')

    <script type="text/javascript">


          $(document).ready(function () {
              $('select[name="country"]').on('change',function () {
                  var countryId = $(this).val();
                  if (countryId)
                  {
                      $.ajax({
                         url: '/city/get/'+countryId,
                         type: "GET",
                         dataType: "json",
                         success: function (data) {

                             $('select[name="city"]').empty();
                             $.each(data, function (key, value) {
                                $('select[name="city"]').append('<option value="'+ key +'">' + value + '</option>');
                             });
                         }
                      });
                  } else {
                      $('select[name="state"]').empty();
                  }
              });
          });

    </script>


    @endsection