@extends('admin.layouts.app')

@section('main-content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
      <h1>
          Restaurants
          <small>safreti</small>
      </h1>
      <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#">Forms</a></li>
          <li class="active">Editors</li>
      </ol>
  </section>

  <!-- Main content -->
  <section class="content">
      <div class="row">
          <div class="col-md-12">

              <div class="box box-primary">
                  <div class="box-header with-border">
                      <h3 class="box-title">Titles</h3>

                  </div>

                  @include('includes.messages')

                  <!-- /.box-header -->
                  <!-- form start -->
                  <form role="form" method="POST" action="{{ route('restaurant.update', $restaurant->id) }}" enctype="multipart/form-data">
                      <div class="box-body">
                          <div class="col-lg-12">

                            {{ csrf_field() }}

                            {{ method_field('PATCH') }}

                              <div class="form-group">
                                  <label for="title">Restaurant Name </label>
                                  <input type="text" class="form-control" id="title" name="name" placeholder="Enter Restaurant" value="{{ $restaurant->name }}">
                              </div>


                              <div class="form-group">
                                  <label for="sub title">Restaurant address</label>
                                  <input type="text" class="form-control" id="address" name="address" placeholder="Enter Address" value="{{ $restaurant->address }}">
                              </div>

                              <div class="form-group">
                                  <label for="slug">Restaurant Description </label>
                                  <input type="text" class="form-control" id="desc" name="desc" placeholder="Enter Description" value="{{ $restaurant->desc }}">
                              </div>

                              <div class="form-group">
                                <label for="slug">Restaurant Mobile </label>
                                <input type="text" class="form-control" id="desc" name="mobile" placeholder="Enter Mobile" value="{{ $restaurant->desc }}">
                            </div>


                            <div class="form-group">
                                    <label>Select Country</label>
                                    <select name="country" class="form-control">
                                            <option>Please Select</option>
                                      @foreach($countries as $country)
                                        <option value="{{ $country->id }}"
                                        @if ($restaurant->country_id == $country->id)
                                            selected
                                            @endif
                                        >{{ $country->name }}</option>
                                        @endforeach
                                    </select>
                                  </div>


                                  <div class="form-group">
                                        <label>Select City</label>
                                        <select name="city" class="form-control">
                                                <option>Please Select</option>
                                        @foreach($cities as $city)
                                            <option value="{{ $city->id }}"

                                                @if($restaurant->city_id == $city->id)
                                                    selected
                                                @endif
                                                > {{ $city->name }}</option>
                                            @endforeach
                                        </select>
                                      </div>

                              <div class="form-group">
                                <label for="image">Restaurant Image</label>
                                <input type="file" name="image" id="image">

                                <p class="help-block">Example block-level help text here.</p>
                                  <img class="img-responsive" height="150" width="150" src="{{ Storage::disk('local')->url($restaurant->image) }}" alt="">
                            </div>

                        <div class="form-group">
                          <button type="submit" class="btn btn-primary">Submit</button>
                          <a type="button" class="btn btn-warning" href="{{ route('restaurant.index') }}">Back</a>
                      </div>
                          </div>
                      </div>
                  </form>
              </div>

          </div>
          <!-- /.col-->
      </div>
      <!-- ./row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->


@endsection
