 <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src=" {{ asset('admin/dist/img/user2-160x160.jpg') }} " class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Safreti</p>

        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview">
          <a href="#">
            <i class="fas fa-city"></i>  <span>Cities</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="{{ route('city.create') }}"><i class="fa fa-circle-o"></i> Add City </a></li>
            <li><a href="{{ route('city.index') }}"><i class="fa fa-circle-o"></i> View Cities</a></li>

          </ul>
        </li>

           <li class=" treeview">
          <a href="#">
            <i class="fas fa-globe-americas"></i>  <span>Countries</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="{{ route('country.create') }}"><i class="fa fa-circle-o"></i> Add Country</a></li>
            <li><a href="{{ route('country.index') }}"><i class="fa fa-circle-o"></i> View Countries</a></li>

          </ul>
        </li>


           <li class="treeview">
          <a href="#">
            <i class="fas fa-map-marker-alt"></i>  <span>Places</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="{{ route('place.create') }}"><i class="fa fa-circle-o"></i> Add Place </a></li>
            <li><a href="{{ route('place.index') }}"><i class="fa fa-circle-o"></i> View Places</a></li>

          </ul>
        </li>

           <li class=" treeview">
          <a href="#">
            <i class="fas fa-utensils"></i>  <span>Restaurants</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="{{ route('restaurant.create') }}"><i class="fa fa-circle-o"></i> Add Restaurant</a></li>
            <li><a href="{{ route('restaurant.index') }}"><i class="fa fa-circle-o"></i> View Restaurants</a></li>

          </ul>
        </li>

        <li class=" treeview">
          <a href="#">
            <i class="fas fa-store"></i>  <span>Shops</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="{{ route('shop.create') }}"><i class="fa fa-circle-o"></i> Add Shop</a></li>
            <li><a href="{{ route('shop.index') }}"><i class="fa fa-circle-o"></i> View Shops</a></li>

          </ul>
        </li>

      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
