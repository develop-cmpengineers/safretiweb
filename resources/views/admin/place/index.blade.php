@extends('admin.layouts.app')

@section('headSection')

<link rel="stylesheet" href="{{ asset('admin/plugins/datatables/dataTables.bootstrap.css') }}">

@endsection

@section('main-content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Blank page
            <small>it all starts here</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Blank page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Title</h3>
                <a type="button"  class="btn btn-success" href="{{ route('place.create') }}">Add Place</a>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body table-responsive">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Name</th>
                            <th>Address</th>
                            <th>Description</th>
                            <th>Mobile</th>
                            <th>Image</th>
                            <th>Country</th>
                            <th>City</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($places as $place)

                        <tr>
                            <td>{{ $loop->index + 1 }}</td>
                            <td>{{ $place->name }}</td>
                            <td>{{ $place->address }}</td>
                            <td>{{ $place->desc }} </td>
                            <td>{{ $place->mobile }}</td>
                            <td><img class="img-responsive" height="100px" width="100px" src="{{ Storage::disk('local')->url($place->image) }}" alt=""></td>
                            <td>

                                @foreach($countries as $country)

                                    @if($place->country_id == $country->id)
                                        {{ $country->name }}
                                    @endif
                                @endforeach

                            </td>
                            <td>
                                @foreach($cities as $city)
                                    @if($place->city_id == $city->id)
                                    {{ $city->name }}
                                    @endif
                                 @endforeach


                            </td>
                            <td><a href="{{ route('place.edit', $place->id) }}"><span class="glyphicon glyphicon-edit"></span></a></td>
                            <td>
                                <form action="{{ route('place.destroy', $place->id) }}" id="delete-form-{{$place->id }}"  method="POST" style="display:none" >
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                </form>

                                <a  onclick="
                                    if(confirm('Are you sure, you want to delete this?')) {
                                        event.preventDefault();
                                        document.getElementById('delete-form-{{$place->id}}').submit();
                                    } else {
                                        event.preventDefault();
                                    } ">
                                    <span class="glyphicon glyphicon-trash"></span>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>

                    <tfoot>
                        <tr>
                            <th>S.No</th>
                            <th>Name</th>
                            <th>Address</th>
                            <th>Description</th>
                            <th>Mobile</th>
                            <th>Image</th>
                            <th>Country</th>
                            <th>City</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                Footer
            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('footerSection')

<script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
</script>

@endsection
