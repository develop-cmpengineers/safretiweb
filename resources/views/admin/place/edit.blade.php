@extends('admin.layouts.app')

@section('main-content')


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
      <h1>
          Places
          <small>safreti</small>
      </h1>
      <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#">Forms</a></li>
          <li class="active">Editors</li>
      </ol>
  </section>

  <!-- Main content -->
  <section class="content">
      <div class="row">
          <div class="col-md-12">

              <div class="box box-primary">
                  <div class="box-header with-border">
                      <h3 class="box-title">Titles</h3>
                  </div>

                    @include('includes.messages')

                  <!-- /.box-header -->
                  <!-- form start -->
                  <form role="form" method="POST" action="{{ route('place.update', $place->id) }}" enctype="multipart/form-data">
                      <div class="box-body">

                        {{ csrf_field() }}
                        {{ method_field('PATCH') }}

                          <div class="col-lg-12">

                              <div class="form-group">
                                  <label for="title">Place Name </label>
                                  <input type="text" class="form-control" id="title" name="name" placeholder="Enter Place" value="{{ $place->name }}">
                              </div>


                              <div class="form-group">
                                  <label for="sub title">Place address</label>
                                  <input type="text" class="form-control" id="address" name="address" placeholder="Enter Address" value="{{ $place->address }}">
                              </div>

                              <div class="form-group">
                                  <label for="slug">Place Description </label>
                                  <input type="text" class="form-control" id="desc" name="desc" placeholder="Enter Description" value="{{ $place->desc }}">
                              </div>

                              <div class="form-group">
                                <label for="slug">Place Mobile </label>
                                <input type="text" class="form-control" id="desc" name="mobile" placeholder="Enter Mobile" value="{{ $place->mobile }}">
                            </div>

                            <div class="form-group">
                                    <label>Select Country</label>
                                    <select name="country" class="form-control">
                                            <option>Please Select</option>
                                        @foreach($countries as $country)
                                        <option value="{{ $country->id }}"

                                        @if($place->country_id == $country->id)
                                            selected
                                            @endif
                                        >{{ $country->name }}</option>
                                            @endforeach
                                    </select>
                                  </div>


                                  <div class="form-group">
                                        <label>Select City</label>
                                        <select name="city" class="form-control">
                                                <option>Please Select</option>
                                        @foreach($cities as $city)
                                            <option value="{{ $city->id }}"
                                            @if($place->city_id == $city->id)
                                            selected
                                            @endif
                                            >{{ $city->name }}</option>
                                        @endforeach
                                        </select>
                                      </div>

                              <div class="form-group">
                                <label for="image">Place Image</label>
                                <input type="file" name="image" id="image">

                                <p class="help-block">Example block-level help text here.</p>
                                  <img class="img-responsive" width="100" height="100" src="{{ Storage::disk('local')->url($place->image)  }}" alt="">
                            </div>

                        <div class="form-group">
                          <button type="submit" class="btn btn-primary">Submit</button>
                          <a type="button" class="btn btn-warning" href="{{ route('place.index') }}">Back</a>
                      </div>
                          </div>
                      </div>
                  </form>
              </div>

          </div>
          <!-- /.col-->
      </div>
      <!-- ./row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection
