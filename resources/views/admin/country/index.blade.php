@extends('admin.layouts.app')

@section('main-content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Blank page
            <small>it all starts here</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Blank page</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Title</h3>
                <a type="button" class="btn btn-success" href="{{ route('country.create') }}"> Add Country</a>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Country Name</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($countries as $country)
                            <tr>
                                <td>{{ $loop->index + 1 }}</td>
                                <td>{{ $country->name }}</td>
                                <td><a href="{{ route('country.edit', $country->id) }}"> <span class="glyphicon glyphicon-edit"></span></a></td>
                                <td>
                                    <form action="{{ route('country.destroy', $country->id) }}" method="POST" id="delete-form-{{ $country->id }}">
                                        {{ csrf_field() }}
                                        {{ method_field('PATCH') }}
                                    </form>
                                    <a href="" onclick="
                                        if(confirm('Are you sure , you want to delete this ? ')) {
                                            event.preventDefault();
                                            document.getElementById('delete-form-{{ $country->id }}').submit();
                                        } else {
                                            event.preventDefault();
                                        }
                                    ">

                                    <span class="glyphicon glyphicon-trash"></span>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>

                    <th>S.No</th>
                    <th>Country Name</th>
                    <th>Edit</th>
                    <th>Delete</th>

                </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                Footer
            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->



@endsection
