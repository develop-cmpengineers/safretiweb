@extends('admin.layouts.app')

@section('main-content')

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Text Editors
                <small>Advanced form element</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Forms</a></li>
                <li class="active">Editors</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">

                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Titles</h3>
                        </div>

                        @include('includes.messages')


                        <!-- /.box-header -->
                        <!-- form start -->
                        <form role="form" method="POST" action="{{ route('country.update', $country->id) }}">
                            <div class="box-body">

                                    {{ csrf_field() }}
                                    {{ method_field('PATCH') }}

                                <div class="col-lg-offset-3 col-lg-6">

                                    <div class="form-group">
                                        <label for="country">Country</label>
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter Country" value="{{ $country->name }}">
                                    </div>


                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        <a type="button" class="btn btn-warning" href="{{ route('country.index') }}">Back</a>
                                    </div>

                                </div>

                            </div>
                            <!-- /.box-body -->
                        </form>
                    </div>

                </div>
                <!-- /.col-->
            </div>
            <!-- ./row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->



@endsection
